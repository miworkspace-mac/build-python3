#!/bin/bash

NEWLOC=`curl -L --compressed https://www.python.org/downloads/macos  -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15' 2>/dev/null| /usr/local/bin/htmlq -a href a | grep .pkg |grep 3.13 | head -1`

# Edit for simultaneous release of new version and old version update

#NEWLOC=`curl -L https://www.python.org/downloads/macos   2>/dev/null | /usr/local/bin/htmlq -a href a | grep .pkg | head -2 | tail -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi