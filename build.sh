#!/bin/bash -ex

# CONFIG
prefix="Python3"
suffix=""
munki_package_name="Python3"
display_name="Python 3"
icon_name=""
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.pkg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
mkdir -p build-root/Applications
/usr/sbin/pkgutil --expand-full "app.pkg" pkg
#app_in_dmg=$(ls -d $mountpoint/*.app)
#cp -R $app_in_dmg build-root/Applications

#(cd build-root; cd Applications; pax -rz -f ../../pkg/Python_Applications*/Payload)

cp -R pkg/Python_Applications.pkg/Payload/Python*  build-root/Applications


# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" build-root/Applications/*/IDLE.app/Contents/Info.plist`
#version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" pkg/Python_Applications.pkg/Payload/Python\ 3.13/IDLE.app/Contents/Info.plist`

#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`

# hdiutil detach "${mountpoint}"

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg --preinstall_script=preinstall_script.sh ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
#/usr/local/munki/makepkginfo app.pkg > app.plist

plist=`pwd`/app.plist

# Obtain version info
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.9.0"
#defaults write "${plist}" minimum_os_version "${minver}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

$HOME/jenkins-trello/otto-notify "${munki_package_name}" "${version}" "${prefix}-${version}.plist" "${minver}" "${maxver}" "${key_files}" "${requires}" "${update_for}"  $*
